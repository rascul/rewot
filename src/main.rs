mod args;
mod client;
mod macros;
mod routes;
mod wot_log;

use actix_files::Files;
use actix_web::{middleware, web, App, HttpServer};

use handlebars::Handlebars;
use log::info;

use client::Client;
use wot_log::WotLog;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
	// parse command line arguments
	let args: args::Args = argh::from_env();

	// start logger
	rag::init().unwrap();
	rag::set_level_debug();

	// load templates
	let mut handlebars = Handlebars::new();
	handlebars.register_templates_directory(".html", "./templates").unwrap();
	let handlebars_ref = web::Data::new(handlebars);

	info!("Configuration (see --help to change):");
	info!("{:?}", args);

	// setup and start http server
	HttpServer::new(move || {
		App::new()
			.app_data(handlebars_ref.clone())
			.wrap(middleware::Logger::default())
			.service(routes::help::get)
			.service(routes::index::get)
			.service(routes::replay::get)
			.service(routes::submit::get)
			.service(routes::submit::post)
			.service(Files::new("/static", "static").show_files_listing())
			.service(Files::new("/replays", "replays"))
	})
	.bind((args.address, args.port))?
	.workers(args.workers)
	.run()
	.await
}
