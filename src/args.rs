use argh::FromArgs;

/// Command line parameters
#[derive(Debug, FromArgs)]
pub struct Args {
	/// ip address to listen on
	#[argh(option, short = 'o', default = "String::from(\"127.0.0.1\")")]
	pub address: String,

	/// port to use
	#[argh(option, short = 'p', default = "8807")]
	pub port: u16,

	/// url to access
	#[argh(option, short = 'u', default = "String::from(\"http://localhost:8807\")")]
	pub url: String,

	/// number of http workers
	#[argh(option, short = 'w', default = "4")]
	pub workers: usize,
}
