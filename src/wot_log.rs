use std::fs::OpenOptions;
use std::io::Write;
use std::path::PathBuf;

use actix_multipart::Multipart;

use chrono::Utc;
use futures_util::StreamExt;
use harsh::Harsh;
use serde_json::{to_string_pretty, Map, Value};

use crate::{client, Client, Result};

#[derive(Debug)]
pub struct WotLog {
	/// ID of the log
	pub id: String,

	/// Title of the log
	pub title: String,

	/// Name of the player
	pub player: String,

	/// Is the log public
	pub public: bool,

	/// Raw bytes of file as received
	pub raw: Vec<u8>,

	/// Client type
	pub client: Client,

	/// Parsed JSON
	pub json: Value,
}

impl WotLog {
	pub fn new() -> Self {
		Self {
			client: Client::None,
			id: {
				let harsh = Harsh::default();
				let ts = Utc::now();
				harsh.encode(&[ts.timestamp_nanos() as u64])
			},
			player: String::new(),
			public: false,
			title: String::new(),
			raw: Vec::new(),
			json: Value::Null,
		}
	}

	pub async fn from_payload(payload: Multipart) -> Result<Self> {
		let mut payload = payload;
		let mut wotlog = WotLog::new();

		while let Some(item) = payload.next().await {
			let mut field = item?;
			let mut data: Vec<u8> = Vec::new();

			while let Some(Ok(chunk)) = field.next().await {
				data.extend_from_slice(&chunk.iter().map(|b| *b).collect::<Vec<u8>>());
			}

			match field.name() {
				"submit_client" => {
					wotlog.client = Client::from(&String::from_utf8_lossy(&data).to_string())
				}
				"submit_player" => wotlog.player = String::from_utf8_lossy(&data).into(),
				"submit_public" => {
					if &data == "public".as_bytes() {
						wotlog.public = true;
					}
				}
				"submit_title" => wotlog.title = String::from_utf8_lossy(&data).into(),
				"submit_file" => wotlog.raw = data.into(),
				_ => {}
			}
		}

		Ok(wotlog)
	}

	/// Read and parse the log
	pub fn parse_log(self: &mut Self) -> Result<()> {
		let mut map: Map<String, Value> = Map::new();
		map.insert("id".into(), self.id.clone().into());
		map.insert("player".into(), self.player.clone().into());
		map.insert("title".into(), self.title.clone().into());

		match self.client {
			Client::Mudlet => {
				let (lines, replay) = client::mudlet::parse_log(&self.raw)?;
				map.insert("lines".into(), lines.into());
				map.insert("replay".into(), replay.into());
			}
			Client::TinTin => {
				let (lines, replay) = client::tintin::parse_log(&self.raw)?;
				map.insert("lines".into(), lines.into());
				map.insert("replay".into(), replay.into());
			}
			Client::ZMud => (),
			Client::None => return Err("no client specified".into()),
		};

		self.json = map.into();

		Ok(())
	}

	/// save the json to disk
	pub fn save(self: &mut Self) -> Result<()> {
		let path = PathBuf::from("replays");

		if !path.is_dir() {
			std::fs::create_dir(&path)?;
		}

		let mut file =
			OpenOptions::new().write(true).create_new(true).open(&path.join(&self.id))?;
		file.write_all(to_string_pretty(&self.json)?.as_bytes())?;

		Ok(())
	}
}
