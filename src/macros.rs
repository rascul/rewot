/// Macro to use error template to return errors
/// must pass it hb then Result
#[macro_export]
macro_rules! render_error {
	($hb: expr, $e:expr) => {
		match $e {
			Ok(r) => r,
			Err(e) => {
				let data = serde_json::json!({
					"error": e.to_string(),
				});
				let body = match $hb.render("error", &data) {
					Ok(r) => r,
					Err(ee) => format!("error: {e}\nerror2: {ee}\n"),
				};
				return Ok(actix_web::HttpResponse::InternalServerError().body(body));
			}
		}
	};
}
