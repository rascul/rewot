use std::ops::Range;

use ansi_to_html::convert_escaped;
use serde_json::{Map, Value};

use crate::Result;

/// try to parse a 32bit int from the delta chunk, then another 32bit in from the size chunk
fn chunk32(chunk: &Vec<u8>) -> Result<(i32, i32)> {
	let (delta_chunk, size_chunk) = {
		let (mut delta_chunk, mut size_chunk): ([u8; 4], [u8; 4]) = ([0; 4], [0; 4]);
		for i in 0..4 {
			delta_chunk[i] = chunk[i];
			size_chunk[i] = chunk[i + 4];
		}
		(delta_chunk, size_chunk)
	};

	let delta = i32::from_be_bytes(delta_chunk);
	let size = i32::from_be_bytes(size_chunk);

	if size < 0 {
		return Err("size is too small".into());
	}

	Ok((delta, size))
}

/// try to parse a 64bit int from the delta chunk, then a 32bit int from the size chunk
fn chunk64(delta_chunk: &Vec<u8>, size_chunk: &Vec<u8>) -> Result<(i32, i32)> {
	let delta = i64::from_be_bytes(delta_chunk.as_slice().try_into()?);
	let size = i32::from_be_bytes(size_chunk.as_slice().try_into()?);

	if delta > i32::MAX.into() {
		return Err("delta out of range".into());
	}

	if size < 1 {
		return Err("size should not be zero".into());
	}

	let delta: i32 = delta.try_into()?;

	Ok((delta, size))
}

/// parse the log of raw bytes and make some nice json for clients
pub fn parse_log(raw: &Vec<u8>) -> Result<(i32, Value)> {
	let mut raw = raw.to_owned();
	let mut chunks: Vec<Map<String, Value>> = Vec::new();
	let mut lineno = 0;

	while raw.len() > 0 {
		// this needs some work because drain can panic and proper checks aren't in place
		let chunk: Vec<u8> = raw.drain(0..8).collect();
		if chunk.len() != 8 {
			return Err("chunk size too small".into());
		}

		let (mut delta, size) = {
			if let Ok((delta, size)) = chunk32(&chunk) {
				(delta, size)
			} else {
				// drain can panic
				let next_chunk: Vec<u8> = raw.drain(0..4).collect();
				if next_chunk.len() != 4 {
					return Err("next_chunk size too small".into());
				}

				if let Ok((delta, size)) = chunk64(&chunk, &next_chunk) {
					(delta, size)
				} else {
					return Err("broken".into());
				}
			}
		};

		let r = Range { start: 0 as usize, end: size as usize };

		if size as usize > raw.len() {
			return Err("Broken!".into());
		}

		// drain can panic
		let text_chunk: Vec<u8> = raw.drain(r).collect();
		if text_chunk.len() != size as usize {
			return Err("text_chunk size too small".into());
		}

		let mut text: String = String::from_utf8_lossy(&text_chunk).into();
		text = convert_escaped(&text)?;

		for line in text.lines() {
			lineno += 1;
			let mut map: Map<String, Value> = Map::new();
			map.insert("lineno".into(), lineno.into());
			map.insert("delta".into(), delta.into());
			map.insert("text".into(), line.trim().into());
			chunks.push(map);
			delta = 0;
		}
	}

	Ok((lineno, chunks.into()))
}
