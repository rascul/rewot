//! Parse the TinTin++ log

use ansi_to_html::convert_escaped;
use chrono::{DateTime, TimeZone, Utc};
use serde_json::{Map, Value};

use crate::Result;

/// Parse the TinTin++ log
///
/// From TinTin++ we can get a timestamp in microseconds from Unix epoch to use for calculating
/// the delta between lines. Each log line shall consist of the timestamp, followed by a space,
/// followed by the the text, possibly with ANSI escape codes or whatever they're called. If there
/// is nothing after the timestamp it should be considered a blank line and should still be
/// put into the resulting `Vec`. The line shall look something like this:
///
///         1452707385328498 [ obvious exits: E ]
///
/// The returned tuple shall be a `i32` for the number of lines and a `serde_json::Value` for
/// the list of parsed lines.
pub fn parse_log(raw: &Vec<u8>) -> Result<(i32, Value)> {
	// v is a Vec to hold all the maps to later process and make it into the json
	let mut v: Vec<Map<String, Value>> = Vec::new();

	// gotta hold the last timestamp so we can calculate the delta from the current timestamp
	let mut last_ts: Option<DateTime<Utc>> = None;

	// the line number is important to track because we need to have it in the json
	let mut lineno = 0;

	// tintin log is by line
	for line in String::from_utf8_lossy(raw).lines() {
		// empty lines ignored
		if line.is_empty() {
			continue;
		}

		// str_ts is the &str version of the timestamp in microseconds
		// msg is the log line
		let (str_ts, msg) = {
			// find the first space because that delimits the ts vs msg
			if let Some(pos) = line.find(" ") {
				line.split_at(pos)
			}
			// only a timestamp means blank line
			else {
				(line, "")
			}
		};

		// we want the timestamp as a 64bit integer so we can make a DateTime thing from it
		let ts_microseconds: i64 = match str_ts.parse() {
			Ok(ts) => ts,
			Err(e) => {
				log::warn!("Error parsing ts: {e}");
				continue;
			}
		};

		// now we can make the DateTime thing so we can do the maths with it later
		let ts = Utc.timestamp_nanos(ts_microseconds * 1000);

		// line number can go in the map now
		lineno += 1;
		let mut map: Map<String, Value> = Map::new();
		map.insert("lineno".into(), Value::Number(lineno.into()));

		// here we can figure out the delta and put it in the map
		if let Some(last_ts) = last_ts {
			let duration = ts - last_ts;
			map.insert("delta".into(), Value::Number(duration.num_milliseconds().into()));
		} else {
			map.insert("delta".into(), Value::Number(0.into()));
		}
		last_ts = Some(ts);

		// put the line into the map after converting the color codes and stuff to html
		map.insert("text".into(), Value::String(convert_escaped(msg)?));

		// push the map we just made to the end of the Vec
		v.push(map);
	}

	Ok((lineno, v.into()))
}
