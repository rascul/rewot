/// mud client type
use std::default::Default;
use std::fmt;

/// client type to know log format
#[derive(Clone, Debug, PartialEq)]
pub enum Client {
	/// mudlet
	Mudlet,

	/// tintin++
	TinTin,

	/// zmud
	ZMud,

	/// no client specified
	None,
}

impl Default for Client {
	fn default() -> Self {
		Client::None
	}
}

impl fmt::Display for Client {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self {
			Self::Mudlet => write!(f, "Mudlet"),
			Self::TinTin => write!(f, "TinTin++"),
			Self::ZMud => write!(f, "ZMud"),
			Self::None => write!(f, "None"),
		}
	}
}

impl From<&String> for Client {
	fn from(s: &String) -> Self {
		match s.to_lowercase().as_str() {
			"mudlet" => Self::Mudlet,
			"tintin" => Self::TinTin,
			"zmud" => Self::ZMud,
			_ => Self::None,
		}
	}
}
