//! submit a log

use actix_multipart::Multipart;
use actix_web::{get, post, web, Error, HttpResponse};

use handlebars::Handlebars;
use serde_json::Value;

use crate::render_error;
use crate::WotLog;

/// get the log submission
#[get("/submit")]
pub async fn get(hb: web::Data<Handlebars<'_>>) -> Result<HttpResponse, Error> {
	Ok(HttpResponse::Ok().body(render_error!(hb, hb.render("submit", &Value::Null))))
}

/// process a submitted log
#[post("/submit")]
pub async fn post(
	payload: Multipart,
	hb: web::Data<Handlebars<'_>>,
) -> Result<HttpResponse, Error> {
	let mut wotlog = render_error!(hb, WotLog::from_payload(payload).await);

	render_error!(hb, wotlog.parse_log());
	render_error!(hb, wotlog.save());

	let res =
		HttpResponse::SeeOther().insert_header(("Location", format!("/+{}", wotlog.id))).finish();

	Ok(res)
}
