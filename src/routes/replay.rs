use std::fs::File;
use std::io::{BufReader, Read};
use std::path::PathBuf;

use actix_web::web::{Data, Path};
use actix_web::{get, HttpResponse};

use handlebars::Handlebars;
use serde_json::json;

use crate::{render_error, Result};

/// load up and play the replay
#[get("/+{id}")]
pub async fn get(id: Path<String>, hb: Data<Handlebars<'_>>) -> Result<HttpResponse> {
	let mut path = PathBuf::from("replays");
	let id: String = id.into_inner();
	path.push(&id);

	let file: File = render_error!(hb, File::open(&path));
	let mut buffer = BufReader::new(file);
	let mut file_contents = String::new();
	render_error!(hb, buffer.read_to_string(&mut file_contents));

	let data = json!({
		"replayid": id,
	});
	let body = render_error!(hb, hb.render("replay", &data));

	Ok(HttpResponse::Ok().body(body))
}
