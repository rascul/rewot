use actix_web::{get, web, Error, HttpResponse};
use handlebars::Handlebars;
use serde_json::Value;

use crate::render_error;

#[get("/help")]
pub async fn get(hb: web::Data<Handlebars<'_>>) -> Result<HttpResponse, Error> {
	Ok(HttpResponse::Ok().body(render_error!(hb, hb.render("help", &Value::Null))))
}
