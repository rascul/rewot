[![Pipeline Status](https://gitlab.com/rascul/rewot/badges/master/pipeline.svg)](https://gitlab.com/rascul/sup/pipelines)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/rascul/sup/blob/master/COPYING)

this is a mud log replay thing

it's not very complete but if you want to hack on it then:

~~~
git clone https://gitlab.com/rascul/rewot
cargo run
~~~

then go to that url

rust docs are at https://rascul.gitlab.io/rewot/doc/rewot

jsdoc at https://rascul.gitlab.io/rewot/jsdoc
