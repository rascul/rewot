/* jshint browser: true */
/* jshint -W097 */
/* global console */

"use strict";

/**
 * rewot.js
 *
 * @file to replay a log
 */

/**
 * a counter for the displayed line we're on
 */
var counter = 1;

/**
 * the main element
 */
var elmain = document.getElementById("main");

/**
 * is the replay going forward or backward?
 */
var direction = "forward";

/**
 * number of lines left to fast forward
 */
var fast_forward_lines = 0;

/**
 * the json format of the log
 */
var json = null;

/**
 * which line of the log we're on
 */
var lineno = 0;

/**
 * when paused this will hold the element displaying paused so that it can be deleted when played
 */
var pausedid = null;

/**
 * are we playing?
 */
var playing = false;

/**
 * multiply the delta by this
 */
var speed = 1;

/**
 * the speed we display, and to get that we multiply display_speed by 2 when we divide speed
 * by 2, and vice versa. since speed actually is a multiplier for the delta it isn't really
 * what we want to see when we want to know how fast things are going
 */
var display_speed = 1;

/**
 * length of time to wait before displaying the next line
*/
var timeout = null;

/**
 * add a line to the display area at the end, if it's visible then go ahead and scroll to it
 *
 * @param {string} line - the logged line of text
 * @param {bool} visible - if visible then the line will be displayed and scrolled to
 */
function add_line(line, visible) {
	// create the element with the proper class and id for the line
	var div = document.createElement("div");
	div.classList.add("replay-div");
	div.id = "replay-div-" + counter;
	div.innerHTML = line || "\n";
	elmain.appendChild(div);

	if (visible) {
		div.classList.add("replay-div-visible");
		scroll(div);
	}

	// i don't understand what's going on here yet
	counter += 1;
	return counter - 1;
}

/**
 * callback to display an error for the xmlhttprequest
 */
function xhr_error_callback() {
	/* jshint validthis: true */
	console.error(this.statusText);
}

/**
 * callback to load the data from the XMLHttpRequest
 */
function xhr_load_callback() {
	/* jshint validthis: true */
	this.callback.apply(this, this.arguments);
}

/**
 * remove a line from the display
 *
 * @param {string} id - id of element to remove
 */
function del_line(id) {
	var el = document.getElementById("replay-div-" + id);
	if (el) {
		el.parentNode.removeChild(el);
	}
}

/**
 * do XMLHttpRequest to fetch the json
 *
 * @param {string} url - url of the file to fetch
 * @param {function} callback - the callback to use when
 */
function fetch_json(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.callback = callback;
	xhr.onload = xhr_load_callback;
	xhr.onerror = xhr_error_callback;
	xhr.open("get", url, true);
	xhr.send(null);
}

/**
 * finished playing the replay so mark it as not playing and send a notice and update the
 * play/pause icon to the paused state, and toggling to play state will replay the replay
 */
function finished() {
	playing = false;
	notice('Finished  <a href="/+' + json.id + '">' +
		(json.title || json.id) + '</a>', true);
	document.getElementById("play_pause").innerHTML = "⏵︎";
}

/**
 * go forwards
 */
function forward() {
	direction = "forward";
	return false;
}

/**
 * hide a line by removing the element's class that makes it visible
 *
 * @param {string} id - element id for the one to hide
 */
function hide_line(id) {
	var el = document.getElementById("replay-div-" + id);
	if (!!el) {
		el.classList.remove("replay-div-visible");
	}
}

/**
 * fetch and load a json log from the given url
 *
 * @param {string} url - url for the json log to fetch and load
 */
function load_log(url) {
	fetch_json(url, load_log_lines_callback);
	return false;
}

/**
 * callback to load the log
 */
function load_log_lines_callback() {
	/* jshint validthis: true */
	json = JSON.parse(this.responseText);

	if (!json) {
		notice("Loading failed", true);
		return;
	}

	var ready = notice(
		(json.title || json.id) + ' loaded  <a href="#" onclick="return play_pause();">Play</a>',
		false
	);

	json.replay.forEach(function(line) {
		json.replay[line.lineno - 1].lineid =
			add_line(line.text, false);
	});

	document.getElementById("progress").max = json.lines;
	document.title = "ReWoT :: " + (json.title || json.id);
	document.getElementById("play_pause").innerHTML = "⏵︎";

	// set lineno to 0 since we just loaded a replay and it hasn't played yet
	document.getElementById("lineno").innerHTML = "0";

	// set num lines with the number of lines in the replay
	document.getElementById("num_lines").innerHTML = json.replay.length;

	update_progress();
	show_line(ready);
}

/**
 * put a notice in the display area
 *
 * @param {string} msg - message to display
 * @param {bool} now - should the message be displayed now (true) or when it's marked visible
 */
function notice(msg, now) {
	var s = '<div class="notice"><span>****</span> ' + msg;
	return add_line(s, now);
}

/**
 * toggle play/pause for a loaded replay
 */
function play_pause() {
	var el = document.getElementById("play_pause");

	if (playing) {
		if (timeout) {
			window.clearTimeout(timeout);
		}
		pausedid = notice("Paused", true);
		playing = false;
		document.getElementById("play_pause").innerHTML = "⏵︎";
	} else {
		if (json) {
			playing = true;
			document.getElementById("play_pause").innerHTML = "⏸︎";
			if (pausedid) {
				del_line(pausedid);
			}
			if (lineno > json.replay.slice(-1)[0].lineno) {
				lineno = json.replay.slice(-1)[0].lineno;
			} else if (lineno < 1) {
				lineno = 1;
			}
			replay_log();
		} else {
			notice("No replay loaded", true);
		}
	}
	return false;
}

/**
 * replay a line from the log
 */
function replay_log() {
	if (!json.replay[lineno] || !playing) {
		return;
	}

	show_line(json.replay[lineno].lineid);

	if (direction == "forward") {
		lineno += 1;
	} else {
		hide_line(json.replay[lineno].lineid);
		lineno -= 1;
	}

	if (json.replay[lineno]) {
		if (fast_forward_lines) {
			fast_forward_lines -= 1;
			replay_log();
		} else {
			var d = parseFloat(json.replay[lineno].delta) * speed;
			if (d > 4000) {
				d = 4000;
			}
			timeout = setTimeout(replay_log, d);
		}
		update_lineno();
		update_progress();
	} else {
		finished();
	}
}

/**
 * scroll to a line, which has its own element `el`
 *
 * @param {object} el - element to scroll to
 */
function scroll(el) {
	var de = document.documentElement;

	// make sure we need to scroll to the element, and if we do, figure out exactly where
	// to scroll to, then scroll there
	if ((el.offsetTop < de.scrollTop) ||
			(el.offsetTop + el.offsetHeight > de.scrollTop + de.clientHeight)) {
		el.scrollIntoView();
	}
}

/**
 * decrease the speed by half which means doubling the delta
 */
function slow_down() {
	speed *= 2;
	display_speed /= 2;
	document.getElementById("speed").innerHTML = display_speed;
	return false;
}

/**
 * increase the speed by two which means halving the delta
 */
function speed_up() {
	speed /= 2;
	display_speed *= 2;
	document.getElementById("speed").innerHTML = display_speed;
	return false;
}

/**
 * go backwards
 */
function reverse() {
	direction = "reverse";
	return false;
}

/**
 * show the indicated line by giving the it appropriate class to make it visible, then scroll to it
 *
 * @param {string} id - element id to make visible
 */
function show_line(id) {
	var el = document.getElementById("replay-div-" + id);
	if (!!el) {
		el.classList.add("replay-div-visible");
		scroll(el);
	}
}

/**
 * update the line number in the display
 */
function update_lineno() {
	document.getElementById("lineno").innerHTML = json.replay[lineno].lineno;
}

/**
 * update the progress bar
 */
function update_progress() {
	var el = document.getElementById("progress");
	el.value = json.replay[lineno].lineno;
	el.title = json.replay[lineno].lineno + " / " + el.max;
}
